import {playerURL,dbURL} from "./constants";

export class Service{

static getUser(username,password){
    return fetch(dbURL+"users")
    .then(res=>res.json())
}
static getPlayers()
{
    return fetch(playerURL)
    .then(resp=>resp.json())
        
}
static getTeam(User)
{
    return fetch(dbURL+"users/"+User['id']+"/team")
    .then(resp=>resp.json())
   
}  
static addUser(User){
    return fetch(dbURL + "users", {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(User)
    })
    .then(res=>res.json())
}   
static addTeam(Team){
    return fetch(dbURL+"team",{
        method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(Team)
    })
    .then(res=>res.json())
}
static createList(obj)
{

}

}